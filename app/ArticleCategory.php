<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleCategory extends Model
{
    protected $table = 'article_category';
    protected $primaryKey = 'id';
    // d
    public Static function articleCategory(){
        $data = ArticleCategory::select('*')
            ->Where('enable', 'Y')
            ->get();
        return $data;
    }
    public Static function articleCategorySelect($id){
        $data = ArticleCategory::select('*')
            ->Where('id', $id)
            ->Where('enable', 'Y')
            ->first();
        return $data->fieldValue;
    }
}
