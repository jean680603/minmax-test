<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
    protected  $table = 'client';
    protected  $primaryKey = 'id';

    public Static function clientIndexs(){
        $data = Client::select('subject','medias','enable','sortIndex')
            ->where('sortIndex','<','30')
            ->whereNotNull('sortIndex')
            ->where('enable','Y')
            ->limit(20)
            ->inRandomOrder()
            ->get();
        return $data;
    }
}
