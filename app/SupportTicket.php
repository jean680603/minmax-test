<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupportTicket extends Model
{
    //
    protected $fillable = ['userInfo', 'serial', 'subject', 'content', 'fields', 'name', 'phone', 'mail', 'mobile'];
    public Static function dataLast(){
        return $data = SupportTicket::select('serial')->last();
    }
}
