<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    protected $table = 'article';
    protected $primaryKey = 'id';
    public function getFirstNameAttribute($value)
    {
        return ucfirst($value);
    }
    // index 倒序
    public Static function workIndexs(){
        $data = Article::select('serial','subject','tags','medias','pageName','enable','sortIndex')
            ->whereNotNull('sortIndex')
            ->Where('enable', 'Y')
            ->orderBy('sortIndex', 'desc')
            ->offset(0)
            ->limit(10)
            ->inRandomOrder()
            ->get();
        return $data;

    }
    // works
    public Static function works(){
        $data = Article::select('serial','subject','tags','medias','pageName','enable','sortIndex','categoryId')
            ->Where('enable', 'Y')
            ->orderBy('sortIndex', 'desc')
            ->get();
        return $data;

    }
    // work 下頁
    public Static function nextPage($sortIndex){
        return $data = Article::select('*')
            ->Where('enable', 'Y')
            ->Where('sortIndex', '<',$sortIndex)
            ->orderBy('sortIndex', 'desc')
            ->limit(1)
            ->first();
    }
    // work 上頁
    public Static function perPage($sortIndex){
        return $data = Article::select('*')
            ->Where('enable', 'Y')
            ->Where('sortIndex', '>',$sortIndex)
            ->orderBy('sortIndex', 'asc')
            ->limit(1)
            ->first();
    }
    // work
    public Static function workDetail($serial){
        $data = Article::select('*')
            ->where('serial','=', $serial)
            ->orWhere('pageName','=', $serial)
            ->Where('enable', 'Y')
            ->first();
        return $data;
    }


}
