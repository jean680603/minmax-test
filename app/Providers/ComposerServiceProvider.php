<?php

namespace App\Providers;
use Illuminate\Support\Facades\Vieww;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        view()->composer('*', 'App\Http\ViewComposers\GlobalComposer');


    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
