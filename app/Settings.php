<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    //
    protected $table = 'settings';

    protected $primaryKey = 'id';

    public Static function settings(){

        return Settings::all();


    }
    public Static function getSettingsFieldValue($fieldName){

        return Settings::where('fieldName', $fieldName)->value('fieldValue');

    }
}
