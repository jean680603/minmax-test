<?php

namespace App\Http\Controllers;



use App\Article;
use App\Http\ViewComposers\GlobalComposer;
use App\Settings;
use App\SupportTicket;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Validator;

class TestController extends Controller
{
    //

    public function test(){
        return view('test.test');
    }

    public function test2(Request $request){
        return view('test.test2');

    }
    public function contactProcess(Request $request){

        $input =$request->all();

        $input['classId'] = "contact";
        $input['userInfo'] = $_SERVER['HTTP_USER_AGENT'];
        $input['serial'] = "C" . substr(date("ymdHis"),0,3).mt_rand(100000,999999);
        $input['fields'] = json_encode($input);
        $input['ip'] = $request->ip();
        $input['time'] = date ("Y- m - d / H : i : s");



        $supportTimcket = SupportTicket::create($input);


        $mail_binding = [
            'websiteTitle' => '聯絡我們',
            'input' =>$input,
        ];
        Mail::send('frontend.mail.supportTicket', $mail_binding,function($mail) use ($input){
            $mail->to($input['mail'])->cc(Settings::getSettingsFieldValue('companyEmail'));

            $mail->subject("已收到您的來信 (".$input['name']." / ".$input['serial'].")");
        });
        return redirect('test/send');


    }
}
