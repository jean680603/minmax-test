<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use League\Glide\Responses\LaravelResponseFactory;
use League\Glide\ServerFactory;
use PHPUnit\Util\Filesystem;

class ImageController extends Controller
{
    //
    public function show(Filesystem $filesystem, $path){
        $server = ServerFactory::create([
            'response' => new LaravelResponseFactory(app('request')),
            'source' => storage_path().'/app/uploads',
            'cache' => storage_path().'/app/cache',
            'cache_path_prefix' => '.cache',
            'storage_path' => 'app/uploads',
        ]);
        return $server->getImageResponse($path, request()->all());
    }
}
