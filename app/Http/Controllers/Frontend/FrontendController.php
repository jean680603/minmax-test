<?php

namespace App\Http\Controllers\Frontend;

use App\Article;
use App\ArticleCategory;
use App\Client;
use App\Http\Controllers\Controller;
use App\Settings;
use App\SupportTicket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Validator;

class FrontendController extends Controller
{


    public function index(){
        $workIndexs = Article::workIndexs();

        foreach ( $workIndexs as $key => $work ) {
            $work['medias']= json_decode ( $work['medias'] );
        }
        $clientIndexs = Client::clientIndexs();

        return view('frontend/index',[
            'workIndexs' => $workIndexs,
            'clientIndexs' => $clientIndexs
        ]);
    }


    public function about(){
        return view('frontend/about');
    }

    public function service(){
        return view('frontend/service');
    }

    public function works(){



       $works = Article::works();
       $categorys = ArticleCategory::articleCategory();
        foreach ( $works as $key => $work ) {
            $work['medias']= json_decode ( $work['medias'] );
            $work['categoryId'] =  ArticleCategory::articleCategorySelect($work['categoryId']);
        }

        $binding = [
            'works' => $works,
            'categorys' => $categorys,
        ];
        return view('frontend/works',$binding);
    }

    public function work($serial){
        $work = json_decode(Article::workDetail($serial));
        if($work) {
            $workMedias = json_decode($work->medias);
            $work->medias= json_decode($work->medias);

            //下頁
            $pageNext =  json_decode(Article::nextPage($work->sortIndex));
            if($pageNext){
                $pageNext->medias =  json_decode($pageNext->medias);
            }

            //下頁
            $perPage =  json_decode(Article::perPage($work->sortIndex));
            if($perPage){
                $perPage->medias =  json_decode($perPage->medias);
            }

            return view('frontend/work',[
                'work' => $work,
                'workMedias' => $workMedias,
                'pageNext' => $pageNext,
                'perPage' => $perPage,
            ]);
        }else{
            return redirect('works');
        }
    }


    public function contact(){
        $binding = [
            'title' => '聯絡我們',
        ];
        return view('frontend.contact',$binding);

    }
    public function contactSend(){
        $binding = [
            'title' => '聯絡我們',
        ];
        return view('frontend.contactSend',$binding);

    }



    public function contactProcess(Request $request){
        $input =$request->all();

        $input['classId'] = "contact";
        $input['userInfo'] = $_SERVER['HTTP_USER_AGENT'];
        $input['serial'] = "C" . substr(date("ymdHis"),0,3).mt_rand(100000,999999);
        $input['fields'] = json_encode($input);
        $input['ip'] = $request->ip();
        $input['time'] = date ("Y- m - d / H : i : s");

        $supportTimcket = SupportTicket::create($input);

        $mail_binding = [
            'title' => '聯絡我們',
            'input' =>$input,
        ];
        Mail::send('frontend.mail.supportTicket', $mail_binding,function($mail) use ($input){
            $mail->to($input['mail'])->cc(Settings::getSettingsFieldValue('companyEmail'));

            $mail->subject("已收到您的來信 (".$input['name']." / ".$input['serial'].")");
        });
        return redirect('contact/send');
    }

}
