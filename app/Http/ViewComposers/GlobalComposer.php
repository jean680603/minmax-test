<?php
namespace App\Http\ViewComposers;

use App\Settings;
use Illuminate\Support\Facades\URL;
use Illuminate\View\View;

class GlobalComposer
{
    protected $settings;

    public function __construct(Settings $settings)
    {
        $this->settings = $settings;

    }

    public function  compose(View $view){


        //網站基本設定
        $websiteName = Settings::getSettingsFieldValue('websiteName');
        $websiteUrl = Settings::getSettingsFieldValue('websiteUrl');
        $websiteEmail = Settings::getSettingsFieldValue('websiteEmail');
        $websiteTitle = Settings::getSettingsFieldValue('websiteTitle');
        $websiteDescription = Settings::getSettingsFieldValue('websiteDescription');
        $websiteKeywords = Settings::getSettingsFieldValue('websiteKeywords');
        $websiteMobile = Settings::getSettingsFieldValue('websiteMobile');
        $companyName = Settings::getSettingsFieldValue('companyName');
        $companyId = Settings::getSettingsFieldValue('companyId');
        $companyEmail = Settings::getSettingsFieldValue('companyEmail');
        $companyPhone = Settings::getSettingsFieldValue('companyPhone');
        $companyFax = Settings::getSettingsFieldValue('companyFax');
        $companyCall = Settings::getSettingsFieldValue('companyCall');
        $companyAddress = Settings::getSettingsFieldValue('companyAddress');
        $gaUaNumber = Settings::getSettingsFieldValue('gaUaNumber');
        $gaGaNumber = Settings::getSettingsFieldValue('gaGaNumber');
        $facebook = Settings::getSettingsFieldValue('facebook');
        $line = Settings::getSettingsFieldValue('line');
        $skype = Settings::getSettingsFieldValue('skype');
        $wechat = Settings::getSettingsFieldValue('wechat');
        $copyright = Settings::getSettingsFieldValue('copyright');
        $socialFacebook = Settings::getSettingsFieldValue('socialFacebook');
        $jobLink = Settings::getSettingsFieldValue('jobLink');

        $view->with('websiteName',$websiteName);
        $view->with('websiteUrl',$websiteUrl);
        $view->with('websiteTitle',$websiteTitle);
        $view->with('websiteEmail',$websiteEmail);
        $view->with('websiteDescription',$websiteDescription);
        $view->with('websiteKeywords',$websiteKeywords);
        $view->with('websiteMobile',$websiteMobile);
        $view->with('companyName',$companyName);
        $view->with('companyId',$companyId);
        $view->with('companyEmail',$companyEmail);
        $view->with('companyPhone',$companyPhone);
        $view->with('companyFax',$companyFax);
        $view->with('companyCall',$companyCall);
        $view->with('companyAddress',$companyAddress);
        $view->with('gaUaNumber',$gaUaNumber);
        $view->with('gaGaNumber',$gaGaNumber);
        $view->with('facebook',$facebook);
        $view->with('line',$line);
        $view->with('skype',$skype);
        $view->with('wechat',$wechat);
        $view->with('copyright',$copyright);
        $view->with('socialFacebook',$socialFacebook);
        $view->with('jobLink',$jobLink);

        $view->with('workDatailName','網頁設計作品');

        //資源路徑
        $publicUrl = 'frontend';
        $view->with('publicUrl',$publicUrl);


        //圖片路徑
        $imgUploads = url('uploads');
        $view->with('img',$imgUploads);

        //public
        $public = storage_path('public');
        $view->with('public',$public);
    }
}