let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
$node_modules='node_modules/';
$modules = 'resources/assets/frontend/js/components/';

function fristCopy(){
    mix.copy($node_modules + 'jquery/dist/jquery.js', $modules + 'jquery/jquery.js');
    mix.copy($node_modules + 'jquery.easing/jquery.easing.min.js', $modules + 'jquery.easing/jquery.easing.js');
    mix.copy($node_modules + 'animate.css/animate.min.css', $modules + 'animate/animate.scss');
    mix.copyDirectory($node_modules + 'imgliquid/js/imgLiquid.js', $modules + 'imgliquid/imgLiquid.js');// 縮圖
    mix.copyDirectory($node_modules + 'slick-carousel/slick', $modules + 'slick');// 輪播
    mix.copyDirectory($node_modules + 'textillate', $modules + 'textillate');// 文字特效
    mix.copy($node_modules + 'skrollr-typed/src/skrollr.js', $modules + 'skrollr/skrollr.js');// 滾動
    mix.copy($node_modules + 'jquery.animate-number/jquery.animateNumber.js', $modules + 'animate-number/jquery.animateNumber.js');// 數字翻動
    mix.copy($node_modules + 'jquery-mousewheel/jquery.mousewheel.js', $modules + 'mousewheel/jquery.mousewheel.js');// 滑鼠事件
    mix.copyDirectory($node_modules + 'malihu-custom-scrollbar-plugin', $modules + 'mCustomScrollbar')
        .copy($node_modules + 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css', $modules + 'mCustomScrollbar/mCustomScrollbar.scss'); // 捲軸
    mix.copyDirectory($node_modules + 'jquery.mb.ytplayer/dist', $modules + 'YTPlayer')
        .copy($node_modules + 'jquery.mb.ytplayer/dist/css/jquery.mb.YTPlayer.min.css', $modules + 'YTPlayer/css/YTPlayer.scss');// 影片播放
    mix.copy($node_modules + 'tinymap/jquery.tinyMap.js', $modules + 'tinymap/jquery.tinyMap.js');// 地圖
    mix.copy($node_modules + 'vide/dist/jquery.vide.min.js', $modules + 'vide/jquery.vide.min.js');// 影片

};
// fristCopy();

mix.sass('resources/assets/frontend/sass/app.scss', 'public/frontend/css')
    //.copy('resources/assets/frontend/js/common/modernizr.js', 'public/frontend/js/modernizr.js')
    .babel([
        $modules + 'jquery/jquery.js',
        $modules + 'jquery.easing/jquery.easing.js',
        $modules + 'imgliquid/imgLiquid.js',
        $modules + 'slick/slick.min.js',
        $modules + 'skrollr/skrollr.js',
        $modules + 'mCustomScrollbar/jquery.mCustomScrollbar.js',
        $modules + 'textillate/assets/jquery.fittext.js',
        $modules + 'textillate/assets/jquery.lettering.js',
        $modules + 'textillate/jquery.textillate.js',
        $modules + 'mousewheel/jquery.mousewheel.js',
        $modules + 'animate-number/jquery.animateNumber.js',
        $modules + 'YTPlayer/jquery.mb.YTPlayer.js',
        $modules + 'tinymap/jquery.tinyMap.js',
        $modules + 'vide/jquery.vide.min.js',
        'resources/assets/frontend/js/common/env.js',
        'resources/assets/frontend/js/main.js'
    ],'public/frontend/js/app.js')
    .version()
    .options({
        postCss: [
            require('autoprefixer')({
                browsers: ['last 2 versions'],
                cascade: false
            })
        ]
    });
