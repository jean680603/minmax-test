<?php


Route::get('/',['as'=>'index','uses' => 'Frontend\FrontendController@index']);
Route::get('about',['as'=>'about','uses' => 'Frontend\FrontendController@about']);
Route::get('service',['as'=>'service','uses' => 'Frontend\FrontendController@service']);
Route::get('works/detail/{serial}',['as'=>'work','uses' => 'Frontend\FrontendController@work']);
Route::get('works',['as'=>'works','uses' => 'Frontend\FrontendController@works']);
//Route::group(['prefix' => 'contact'], function (){
//    Route::get('/','Frontend\FrontendController@contact');
//    Route::post('/','Frontend\FrontendController@contactProcess');
//    Route::get('/send','Frontend\FrontendController@contactSend');
//});

Route::prefix('contact')->middleware('web')->group(function () {
    Route::get('/','Frontend\FrontendController@contact');
    Route::post('/','Frontend\FrontendController@contactProcess');
    Route::any('/send','Frontend\FrontendController@contactSend');
});

Route::get('/uploads/{path}', 'ImageController@show')->where('path', '.*');


Route::prefix('test')->middleware('web')->group(function () {
    Route::post('/',['as'=>'test','uses' => 'TestController@contactProcess']);
    Route::get('/',['as'=>'test','uses' => 'TestController@test']);
    Route::any('send',['as'=>'test2','uses' => 'TestController@test2']);
});
