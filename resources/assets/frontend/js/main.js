$(document).ready(function () {
    if (!/iPhone|iPad|iPod|ios/i.test(window.navigator.userAgent)) {
        $(".wp").on('mousewheel', function (event, delta) {
            $('html,body').stop();
            if (!$("html").hasClass("macintosh")) {
                var that = this,
                    duration = 800,
                    easing = 'easeOutCirc',
                    step = 80,
                    target = $('html, body'),
                    scrollHeight = $(document).height(),
                    scrollTop = that.last !== undefined ? that.last : $(window).scrollTop(),
                    viewportHeight = $(window).height(),
                    multiply = (event.deltaMode === 1) ? step : 1;

                scrollTop -= delta * multiply * step;
                scrollTop = Math.min((scrollHeight - viewportHeight), Math.max(0, scrollTop));
                that.last = scrollTop;

                target.stop().animate({
                    scrollTop: scrollTop
                }, {
                    easing: easing,
                    duration: duration,
                    complete: function () {
                        delete that.last;
                    }
                });
                event.preventDefault();
            }
        });
    } else {
        $(".wp").on('mousewheel', function () {
            $('html,body').stop();
        });
    }

    if (!/Android/i.test(window.navigator.userAgent)) {
        $('.scrollbarX').mCustomScrollbar({
            axis: "x",
            mouseWheel: { enable: false },
        });
    }

    // $('.scrollbar').mCustomScrollbar({
    //     axis: "y",
    //     //autoHideScrollbar: true,
    // });

    $('.jqimgFill').imgLiquid();
    $('.jqimgFill, .wp').css("opacity", "1");

    $(document).on('click', 'input[type=text]', function () {
        this.select();
    });

    if ($("html").hasClass('min-lg-size')) {
        var skrollr_obj = skrollr.init();
    }

    $(".goTop").each(function () {
        $(this).click(function () {
            $("html,body").animate({ scrollTop: 0 }, 1500, 'easeInOutCubic');
            return false;
        });
    });
    $(".goDown").each(function () {
        $(this).click(function () {
            $("html,body").animate({ scrollTop: $(".scroll-here").offset().top }, 1300, 'easeInOutCubic');
            return false;
        });
    });

    /* ==========================================================================
       open-box
      ==========================================================================*/
    $(".open-box").each(function () {
        var fixedBox = $(this).attr("data-box");
        $(this).on("click tap", function () {
            $("#" + fixedBox).addClass("show");
            $(".fixed-box .scollbar").mCustomScrollbar("scrollTo", "top", { scrollInertia: 1 });
            $("body").addClass("disable");
        });

        $(".closeBox").on("click tap", function () {
            $(".fixed-box").removeClass("show");
            $("body").removeClass("disable");
        });

        $(".changeImg").on("click tap", function () {
            var imgSrc = $(this).children("img").attr("src");
            $("#zoom-pic img").attr("src", imgSrc);
            // console.log(imgSrc)
        });

        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            var $docEl = $('html, body'),
                $wrap = $('.wp'),
                scrollTop;
            $('.open-box').click(function (e) {
                overlayOpen();
                e.preventDefault();
            });
            $('.closeBox').click(function (e) {
                overlayClose();
                e.preventDefault();
            });
            var overlayClose = function () {
                $.unlockBody();
            }
            var overlayOpen = function () {
                $.lockBody();
            }
            $.lockBody = function () {
                if (window.pageYOffset) {
                    scrollTop = window.pageYOffset;
                    $wrap.css({
                        top: - (scrollTop)
                    });
                }
                $docEl.css({
                    height: "100%",
                    overflow: "hidden"
                });
            }
            $.unlockBody = function () {
                $docEl.css({
                    height: "",
                    overflow: ""
                });
                $wrap.css({
                    top: ''
                });
                window.scrollTo(0, scrollTop);
                window.setTimeout(function () {
                    scrollTop = null;
                }, 0);

            }
        }
    });

    /* ==========================================================================
        [header]
     ==========================================================================*/
    $('#header').each(function () {
        $(this).css("visibility", "visible");
        $(".menu-toggle").click(function () {
            $("#header").toggleClass('active');
        });
        $(this).find('.slider-pic').slick({
            dots: false,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            speed: 1000,
            cssEase: 'ease',
            easing:'easeInOutCirc',
            autoplaySpeed: 3000,
            pauseOnHover: false
        });

        var page = $(".wp").attr("id");
        $("#" + page).find('.menu li[data-nav="' + page + '"]').addClass('active');

        $(window).scroll(function () {
            var winTop = $(window).scrollTop();
            if ($(this).scrollTop() > 200) {
                $('#header').addClass("pack-up");
            } else {
                $('#header').removeClass("pack-up");
            };
        });
    });

    $(document).mouseup(function (e) {
        var _con = $("#header");
        if (!_con.is(e.target) && _con.has(e.target).length === 0) {
            $("#header").removeClass("active");
        }
    });

    $(document).ready(function () {
        var $menu = $('#header');
        $menu.length && $(window).scroll(function () {
            var scrollTop = $menu.offset().top;
            $('.light, .dark').each(function () {
                var me = $(this),
                    top = me.offset().top - $("#header").outerHeight(),
                    bottom = top + me.outerHeight(true);

                if (scrollTop > top && scrollTop < bottom) {
                    $(".wp").toggleClass('header-light', me.hasClass('dark'));
                    return false;
                }
            });
        })
            .scroll();
    });

    /* ==========================================================================
            文字特效
    ==========================================================================*/
    $('.tlt-run').textillate({
        loop: false,
        in: {
            effect: 'flipInY',
        },
    });

    $('.tlt').each(function () {
        var check = true;
        var tlt_item = $(this);
        $(window).scroll(function () {
            var winTop = $(window).scrollTop() + $(window).height() - $(window).height() / 4;
            var pageTop = tlt_item.offset().top;
            if (winTop > pageTop && check) {
                tlt_item.css('opacity', 1).textillate({
                    loop: false,
                    in: {
                        effect: 'flipInY',
                    },
                });
                check = false;
            }
        });
    });

    $('.tlt-loop').each(function () {
        var check = true;
        var tlt_item = $(this);
        $(window).scroll(function () {
            var winTop = $(window).scrollTop() + $(window).height() - $(window).height() / 4;
            var pageTop = tlt_item.offset().top;
            if (winTop > pageTop && check) {
                tlt_item.css('opacity', 1).textillate({
                    loop: true,
                    in: {
                        effect: 'flipInY',
                        delay: 50,
                    },
                    out: {
                        effect: 'fadeOutBlur',
                        delay: 50,
                        shuffle: true,
                    },
                });
                check = false;
            }
        });
    });

    /* ==========================================================================
      * 頁面區塊
    ==========================================================================*/
    $('.run-number').each(function () {
        var check = true;
        var number_value = $(this).attr("data-number");
        var number_item = $(this);
        $(window).scroll(function () {
            var winTop = $(window).scrollTop() + $(window).height() - $(window).height() / 4;
            var pageTop = number_item.offset().top;
            if (winTop > pageTop && check) {
                number_item.animateNumber({
                    number: number_value
                }, 1000);
                check = false;
            }
        });
    });

    $(".bg-video").each(function () {
        var $this = $(this);
        $this.YTPlayer({
            mute: true,
            autoPlay: true,
            videoURL: 'https://www.youtube.com/watch?v=ab0TSkLe-E0',
            //videoURL: 'https://www.youtube.com/watch?v=b8-3_Etyc1o',
            useOnMobile: false,
            startAt: 0
        });
        $this.show();
        // if ( $("html").hasClass("no-touch") ) {
        //     setTimeout(function () {
        //         $this.fadeOut(1000);
        //     }, 1500);
        // }
    });

    $(".has-animation li, .go").each(function () {
        var item = $(this);
        $(window).scroll(function () {
            var winTop = $(window).scrollTop() + $(window).height() - $(window).height() / 5;
            var pageTop = item.offset().top;
            if (winTop >= pageTop) {
                item.addClass("show");
            } else {
                item.removeClass("show");
            }
        });
    });

    $("ul.types > li").each(function () {
        var type = $(this);
        var _clickTab = type.attr('data-type');
        type.click(function () {
            $(this).addClass('active').siblings('.active').removeClass('active');
            $(".work li").hide();
            $(".work li." + _clickTab).fadeIn();
            //$(".project li." + _clickTab).fadeIn().siblings().hide();

            var node = $(".work li." + _clickTab);
            node.insertBefore($(".work ul li:first-of-type") );
            //document.getElementById("por-list").appendChild(node);
        });
    });

    /*== footer  =========================== */
    $("#footer").each(function () {
        $(".fixed-tools .toggle").click(function () {
            $(this).next("ul").toggleClass("show");
        });
        $(window).scroll(function () {
            var pageTop = $('.main').offset().top;
            var pageBottom = $(document).height() - $(window).height() - $("#footer").height();
            var winTop = $(window).scrollTop();
            if (winTop >= pageTop && winTop <= pageBottom) {
                $('.fixed-tools').removeClass("active");
            } else {
                $('.fixed-tools').addClass("active");
            }
            if ($(this).scrollTop() > 200) {
                $('.fixed-tools').fadeIn();
            } else {
                $('.fixed-tools').fadeOut();
            };
        });
        if ($("html").hasClass('min-sm-size')) {
            $(window).on("mousewheel", function () {
                $('.fixed-tools ul').removeClass("show");
            });
        }
    });

    /*== main  =========================== */
    $(".home-banner").each(function () {
        $(this).find('.slider').slick({
            dots: false,
            arrows: true,
            autoplay: true,
            speed: 1500,
            cssEase: 'ease',
            easing: 'easeInOutCirc',
            autoplaySpeed: 3000,
            appendArrows:$(".banner .arrows"),
            pauseOnHover: false,
            //adaptiveHeight: true
        });
    });

    $(".client").each(function () {
        $(this).find('.slider').slick({
            dots: false,
            arrows: true,
            slidesToShow: 5,
            slidesToScroll: 1,
            rows: 2,
            autoplay: true,
            speed: 1000,
            cssEase: 'ease',
            easing:'easeInOutCirc',
            autoplaySpeed: 1500,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 4,
                    }
                }, {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 3,
                    }
                }, {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                    }
                }
            ]
        });
    });

    $(".our-team").each(function () {
        var item = $(this).find('ul.photos li');
        $(window).scroll(function () {
            var winTop = $(window).scrollTop() + $(window).height() - $(window).height() / 4;
            var pageTop = item.offset().top;
            if (winTop >= pageTop) {
                item.each(function (i) {
                    var $this = $(this);
                    setTimeout(function () {
                        $this.addClass("show");
                    }, (i + 1) * 100 + 300);
                });
            } else {
                item.removeClass("show");
            }
        });
    });

    $(".project-mac").each(function () {
        $(this).find('.slider').slick({
            dots: false,
            arrows: false,
            autoplay: true,
            speed: 1500,
            fade: true,
            autoplaySpeed: 2000,
            appendArrows: $(".project-mac .block"),
            pauseOnHover: false,
            responsive: [
                {
                    breakpoint: 1600,
                    settings: {
                        appendArrows: $(".project-mac .mac"),
                    }
                }, {
                    breakpoint: 480,
                    settings: {
                        appendArrows: $(".project-mac .mac"),
                        arrows: false,
                    }
                }
            ]
        });
    });

    $(".project-page").each(function () {
        $(".project-page .pic:first-of-type").addClass("active");
        $(".project-page .pic").hover(function () {
            $(this).addClass("active").siblings().removeClass("active");
        });
    });

    $("#projects").each(function () {
        $(window).scroll(function () {
            var pageTop = $('.project-mac').offset().top;
            var pageBottom = $(document).height() - $(window).height() - $(".project").height();
            var winTop = $(window).scrollTop();


            if (winTop >= pageTop && winTop <= pageBottom) {
                $('.prevNext').fadeIn();
            } else {
                $('.prevNext').fadeOut();
            }
        });
    });


    $(".map").each(function () {
        $(this).tinyMap({
            'center': [24.1717572, 120.7060358],
            'zoom': 15,
            'marker': [
                {
                    'addr': [24.1717572, 120.7060358],
                    'text': '<strong>406 台中市北屯區東山路一段 147 巷 49 號</strong>',
                    //'newLabel': '雲端數位科技有限公司',
                    //'newLabelCSS': 'labels',
                    'icon': {
                        'url': '/images/map-icon.png',
                    },
                    'animation': 'DROP'
                },
            ],
            'styles': [{ "featureType": "administrative", "elementType": "geometry", "stylers": [{ "saturation": "2" }, { "visibility": "simplified" }] }, { "featureType": "administrative", "elementType": "labels", "stylers": [{ "saturation": "-28" }, { "lightness": "-10" }, { "visibility": "on" }] }, { "featureType": "administrative", "elementType": "labels.text.fill", "stylers": [{ "color": "#444444" }] }, { "featureType": "landscape", "elementType": "all", "stylers": [{ "color": "#f2f2f2" }] }, { "featureType": "landscape", "elementType": "geometry.fill", "stylers": [{ "saturation": "-1" }, { "lightness": "-12" }] }, { "featureType": "landscape.natural", "elementType": "labels.text", "stylers": [{ "lightness": "-31" }] }, { "featureType": "landscape.natural", "elementType": "labels.text.fill", "stylers": [{ "lightness": "-74" }] }, { "featureType": "landscape.natural", "elementType": "labels.text.stroke", "stylers": [{ "lightness": "65" }] }, { "featureType": "landscape.natural.landcover", "elementType": "geometry", "stylers": [{ "lightness": "-15" }] }, { "featureType": "landscape.natural.landcover", "elementType": "geometry.fill", "stylers": [{ "lightness": "0" }] }, { "featureType": "poi", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "road", "elementType": "all", "stylers": [{ "saturation": -100 }, { "lightness": 45 }] }, { "featureType": "road", "elementType": "geometry", "stylers": [{ "visibility": "on" }, { "saturation": "0" }, { "lightness": "-9" }] }, { "featureType": "road", "elementType": "geometry.stroke", "stylers": [{ "lightness": "-14" }] }, { "featureType": "road", "elementType": "labels", "stylers": [{ "lightness": "-35" }, { "gamma": "1" }, { "weight": "1.39" }] }, { "featureType": "road", "elementType": "labels.text.fill", "stylers": [{ "lightness": "-19" }] }, { "featureType": "road", "elementType": "labels.text.stroke", "stylers": [{ "lightness": "46" }] }, { "featureType": "road.highway", "elementType": "all", "stylers": [{ "visibility": "simplified" }] }, { "featureType": "road.highway", "elementType": "labels.icon", "stylers": [{ "lightness": "-13" }, { "weight": "1.23" }, { "invert_lightness": true }, { "visibility": "simplified" }, { "hue": "#ff0000" }] }, { "featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "water", "elementType": "all", "stylers": [{ "color": "#adadad" }, { "visibility": "on" }] }]
        });
    });

    /* ==========================================================================
            IE 9 不支援 placeholder
    ==========================================================================*/
    (function ($) {
        $.support.placeholder = ('placeholder' in document.createElement('input'));
    })(jQuery);

    //fix for IE7 and IE8
    $(function () {
        if (!$.support.placeholder) {
            $("[placeholder]").focus(function () {
                if ($(this).val() == $(this).attr("placeholder")) $(this).val("");
            }).blur(function () {
                if ($(this).val() == "") $(this).val($(this).attr("placeholder"));
            }).blur();

            $("[placeholder]").parents("form").submit(function () {
                $(this).find('[placeholder]').each(function () {
                    if ($(this).val() == $(this).attr("placeholder")) {
                        $(this).val("");
                    }
                });
            });
        }
    });

    /* ==========================================================================
      * 螢幕縮放動作
    ==========================================================================*/
    $(window).resize(function () {
        resizeCss();
    });

    function resizeCss() {

    }
    /*document END*/
});
