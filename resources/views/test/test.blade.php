<form class="form" action="test" method="POST">
    @csrf
    <div class="group">
        <label for="inputName" class="label">姓名 <span class="red">*</span></label>
        <input type="text" class="right form-control"  name="name" required="required" value="{{ old('name') }}">
    </div>
    <div class="group">
        <label for="inputCompany" class="label">公司 <span class="red">*</span></label>
        <input type="text" class="right form-control"  name="company"  value="{{ old('company') }}">
    </div>
    <div class="group">
        <label for="inputTel" class="label">電話 <span class="red">*</span></label>
        <input type="text" class="right form-control"  name="phone" required="required" value="{{ old('phone') }}">
    </div>

    <div class="group">
        <label for="inputMail" class="label">E-mail <span class="red">*</span></label>
        <input type="text" class="right form-control"  name="mail" required="required" value="{{ old('mail') }}">
    </div>
    <div class="group">
        <label for="inputName" class="label">主旨 <span class="red">*</span></label>
        <select name="subject" class="right form-control" id="selectItem" required="required">
            <option value="網站製作">網站製作</option>
            <option value="系統開發">系統開發</option>
            <option value="售後服務">售後服務</option>
            <option value="其他">其他</option>
        </select>

    </div>
    <div class="group">
        <label for="inputText" class="label">留言 <span class="red">*</span></label>
        <textarea name="content"  value="{{ old('content') }}" class="right form-control" id="content" rows="5" placeholder="我們的目標是與合作夥伴建立長期的合作關係。詳細描述您當前的需求以及您可能對此感興趣的內容。" required="required"></textarea>
    </div>
    <div class="row mt-5 buttonBlock">
        <div class="col text-center text-sm-left ml-0 ml-md-3"><div class="g-recaptcha" data-sitekey="6LftW2QUAAAAAOqPoOfbK9ScH8peSk-EGrK2TID_"></div></div>
        <div class="col text-center text-sm-right mt-3"><button type="submit" class="button2 bling">送出</button></div>
    </div>
</form>