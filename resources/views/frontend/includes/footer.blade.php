<footer id="footer" class="animated fadeInDown">
    <div class="container">
        <address class="address row">
                <div class="col-md-auto mr-3 text-center"> <a href="./" class="logo d-inline-block " title="MINMAX"><img src="images/logo.png" alt=""></a></div>
                <div class="col text-md-left mr-3 text-center">
                    <div class="row py-0">
                        <p class="col-md-auto mb-0">TEL：<a href="tel:{{$companyPhone}}">{{$companyPhone}}</a></p>
                        <p class="col-md-auto mb-0">FAX：{{$companyFax}}</p>
                        <p class="col-md-auto mb-0">E-MAIL：<a href="mailto:{{$companyEmail}}">{{$companyEmail}}</a></p>
                    </div>
                    <div class="row">
                        <p class="col-md-auto mb-0"><a href="https://goo.gl/maps/Vgb8xTW225R2" target="_blank" >{{$companyAddress}}<i class="icon-map text-danger ml-2 h5"></i></a></p>
                        <p class="col-md-auto mb-0">統一編號：{{$companyId}}</p>
                    </div>
                </div>
                <div class="col-md-auto mt-3 mt-md-0">
                    <a href="{{$socialFacebook}}" title="Facebook" class="follow mr-2" target="_blank">
                        <i class="icon-facebook2"></i>
                        <span class="d-none">Facebook</span>
                    </a>
                    <a href="contact" title="Contact Us" class="follow">
                        <i class="icon-mail"></i>
                        <span class="d-none">Contact Us</span>
                    </a>
                </div>
        </address>
        <p class="copyright">{{$copyright}}</p>
    </div>
    <div class="fixed-tools">
        <div class="toggle" title="線上即時通"><i class="icon-comment"></i><span class="d-none">線上即時通</span></div>
        <ul class="list-unstyled">
            <li>
                <a class="box messenger" href="{{$facebook}}" target="_blank" title="Facebook Messenger">
                    <i class="icon-message"></i>
                    <span class="d-none">Facebook Messenger</span>
                </a>
            </li>
            <li>
                <div class="box wechat open-box" title="Wechat" data-box="qrcode">
                    <i class="icon-wechat"></i>
                    <span class="d-none">Wechat</span>
                </div>
            </li>
            <li>
                <a class="box skype skype-button" href="{{$skype}} title="Skype">
                    <i class="icon-skype"></i>
                    <span class="d-none">Skype</span>
                </a>
            </li>
            <li>
                <a class="box line" href="{{$line}}" target="_blank" title="Line">
                    <i class="icon-line"></i>
                    <span class="d-none">Line</span>
                </a>

            </li>
        </ul>
        <div class="goTop"><i class="icon-up"></i></div>
    </div>
</footer>
<div class="fixed-box" id="qrcode">
    <div class="mask closeBox"></div>
    <div class="inner">
        <div class="close closeBox" title="關閉" id="close"><i class="icon-close"></i></div>
        <div class="scrollbar">
            <div class="pic">
                <img src="{{$img}}/qrcode.jpg" draggable="false" alt="雲端數位 - 微信 QRCODE">
            </div>
        </div>
    </div>
</div>