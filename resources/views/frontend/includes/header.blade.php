<header id="header" class="animated fadeInDown">
    <div class="logo">
        <a href="/"><h1 class="d-none">{{$websiteName}}</h1></a>
    </div>
    <div class="menu-toggle">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
    </div>
    <a href="tel:0424350749" class="tel"><i class="icon-phone"></i></a>
    <div class="menu scrollbar">
        <div class="list">
            <ul class="middle list-unstyled">
                <li data-nav="about"><a href="/about" title="關於我們">ABOUT US</a></li>
                <li data-nav="works"><a href="/works" title="作品賞析">PROJECTS</a></li>
                <li data-nav="service"><a href="/service" title="服務項目">SERVICE</a></li>
                <li data-nav="contact"><a href="/contact" title="聯絡雲端">CONTACT US</a></li>
                <li><a href="https://www.104.com.tw/jobbank/custjob/index.php?r=cust&j=4070432838363f6940583a1d1d1d1d5f2443a363189j48" target="_blank" title="人才招募">JOIN US</a></li>
            </ul>
        </div>
        <div class="view">
            <ul class="slider-pic list-unstyled">
                <li>
                    <a href="projects" title="精選作品">
						<span class="pic jqimgFill">
							<img src="storage/app/uploads/home/view.jpg" draggable="false" alt="精選作品">
						</span>
                    </a>
                </li>
                <li>
                    <a href="projects" title="精選作品">
						<span class="pic jqimgFill">
							<img src="storage/app/uploads/home/view.jpg" draggable="false" alt="精選作品">
						</span>
                    </a>
                </li>
            </ul>
            <div class="text">
                <a href="projects">
                    <span class="title">精選作品</span>
                    <span class="eng2">VIEW MORE</span>
                </a>
            </div>
        </div>
    </div>
</header>