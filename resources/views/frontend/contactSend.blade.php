@extends('frontend.layouts.app')
@section('wpClass', 'header-light')
@section('title', $title)
@section('pageId', 'contact')
@section('content')
    <section class="dark-banner dark">
        <div class="bg full">
            <div class="move jqimgFill" data-bottom-top="transform:translateY(-30%);" data-top-center="transform:translateY(10%);">
                <img src="{{$img}}/contact/banner-bg.jpg" draggable="false" alt="Contact Us 聯絡雲端">
            </div>
        </div>
        <div class="container">
            <h2 class="d-none">Contact Us 聯絡雲端</h2>
            <div class="middle">
                <div class="title eng2 display-2 tlt-loop">Contact Us</div>
                <div class="text">想進一步瞭解 MINMAX 的產品或服務， 或是對 MINMAX 有任何寶貴意見，都歡迎您與我們聯繫，我們將竭誠為您服務！ </div>
            </div>
        </div>
    </section>
    <section class="form-block light">
        <div class="container">
                <div class="h3 text-center pb-4">您的資料已經送出，我們將盡快為您服務處理！</div>
                <div class="mt-5  text-center">
                    <a href="/contact" class="button bling go fade-up show">重新填寫</a>
                    <a href="/works" class="button bling go fade-up show">觀看我們的作品</a>
                </div>
        </div>
    </section>
    <div class="map"></div>
@endsection