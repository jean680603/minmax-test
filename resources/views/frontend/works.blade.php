@extends('frontend.layouts.app')
@section('wpClass', '')
@section('pageId', 'projects')
@section('content')
    <section class="slogan-info light">
        <div class="container">
            <h2 class="d-none">Case Overview</h2>
            <div class="middle">
                <div class="title eng2 display-2 tlt-loop" data-bottom-top="transform:translateY(-200px);" data-top-center="transform:translateY(50px);">Case Overview</div>
                <div class="cn h1 tlt-run" data-bottom-top="transform:translateY(100px);" data-top-center="transform:translateY(-50px);">量身打造全新的UI及用户體驗設計</div>
                <ul class="tabs types list-unstyled go fade-up">
                    <li class="active" data-type="all">ALL</li>
                    @foreach ($categorys as $category)
                    <li data-type="{{$category->fieldValue}}">{{$category->subject}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </section>
    <section class="work has-animation dark">
        <h2 class="d-none">Projects 作品賞析</h2>
        <ul class="list list-unstyled clearfix">
            @foreach ($works as $work)
                <li class="all {{ $work->categoryId }}">
                    <a class="box" href="./works/detail/{{ $work->serial }}" title="{{$companyName}} - {{ $work->subject }}">
                        <span class="box-line"></span>
                        <span class="pic img jqimgFill">
                         <img  src="{{ $img }}/{{ $work->medias->list[0]->fileName  or $work->medias->banner[0]->fileName}}?w=600&fit=crop" draggable="false" alt="{{$workDatailName}} - {{ $value->title or $work->subject }}">
                        </span>
                    </a>
                    <div class="text">
                        <h3 class="name h2">{{ $work->subject }}</h3>
                        <span class="summary d-none d-md-inline-block">{{ $work->tags }}</span>
                    </div>
                </li>
            @endforeach
        </ul>
    </section>
@endsection