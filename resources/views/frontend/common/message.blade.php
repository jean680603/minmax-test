@if (Session::has('success'))
<div class="alert alert-success" role="alert">
    <strong>@yield('success','成功')</strong>！{{ Session::get('success') }}
</div>
@endif
@if (Session::has('failure'))
<div class="alert alert-danger" role="alert">
    <strong>@yield('failure','失敗')</strong>！{{ Session::get('failure') }}
</div>
@endif
