@extends('frontend.layouts.app')
@section('wpClass', '')
@section('pageId', 'about')
@section('content')
<div class="slogan-info gray-bg">
    <div class="container">
        <div class="decoration">
            <div class="left-tri" data-bottom-top="transform:translateX(10%);" data-top-center="transform:translateX(-30%);"></div>
            <div class="right-line" data-bottom-top="transform:translateY(30%);" data-top-center="transform:translateY(0%);"></div>
            <div class="right-tri" data-bottom-top="transform:translateX(-30%);" data-top-center="transform:translateX(10%);">
                <div class="sub-tri" data-bottom-top="transform:translateY(-150px);" data-top-center="transform:translateY(100px);"></div>
            </div>
            <div class="bottom-line" data-bottom-top="transform:translateY(-50px);" data-top-center="transform:translateY(100px);"></div>
        </div>
        <h2 class="d-none">Inspire Change</h2>
        <div class="middle">
            <div class="title eng2 display-2 tlt-loop" data-bottom-top="transform:translateY(-200px);" data-top-center="transform:translateY(50px);">Inspire Change</div>
            <div class="cn h1 tlt-run">知識、經驗與創新的想法，我們知道你需要甚麼！</div>
            <div class="text h4 go fade-in" data-bottom-top="transform:translateY(100px);" data-top-center="transform:translateY(-50px);">10 年來，我們積累了豐富的管理和創意經驗，<br>我們樂於分享，激勵，創新，為合作企業創造更大的商業價值。</div>
        </div>
    </div>
</div>
<div class="we-do light">
    <div class="top clearfix">
        <h2 class="title eng2 display-2" data-bottom-top="transform:translateX(100px);" data-top-center="transform:translateX(-50px);">
            <span class="go fade-right"><span class="red">We</span> create</span>
            <span class="go fade-right"><span class="red">We</span> think</span>
            <span class="go fade-right"><span class="red">We</span> design</span>
        </h2>
        <div class="ipad">
            <div class="pic" data-bottom-top="transform:translateY(100px);" data-top-center="transform:translateY(-50px);">
                <img src="{{$img}}/about/ipad.png" draggable="false" alt="We create, We think, We design">
            </div>
            <div class="tri" data-bottom-top="transform:translateY(-50px);" data-top-center="transform:translateY(0px);"></div>
        </div>
    </div>
    <div class="article">
        <div class="tir" data-bottom-top="transform:translateX(-100px);" data-top-center="transform:translateX(0px);"></div>
        <div class="container">
            <ul class="list list-unstyled">
                <li>
                    <div class="text go fade-up">雲端數位科技最早起萌於 2008 年的盛夏，我們是一家充滿新鮮創意及設計熱情的網路公司。＂臻至完美、近乎苛求＂恰如其分的為雲端數位科技寫實了經營理念，多年以來，我們培育與精鍊出了一群優秀聲譽、頂尖質量的好手，並且橫跨多方面領域的設計團隊，我們擅於打造以創新為本的設計作品、專業於多種領域的軟體工程技術及全方位的服務導向整合，雲端數位科技總是企圖將創意發揮到淋漓盡現，並且努力使命必達，我們深信知識與經驗的累積才是打動人心的服務。</div>
                    <div class="pic full jqimgFill go fade-up">
                        <img src="{{$img}}/about/we-do01.jpg" draggable="false" alt="We create, We think, We design">
                    </div>
                </li>
                <li>
                    <div class="text go fade-up">然而在雲端數位科技，我們對於這裡的所有成員，無論他們所負責的部門職責為何，都期待大家具備創意思維和解決方案。創新的形式有許多種，但我們的開發人員總是能在不斷的腦力激盪下創造出無限可期的精彩新意。對於設計美學的堅持，以及在這裡所面對的每一個專案執行，我們希望將品牌設計理念傳達給消費者，設計是無形的品味，更是具體的視覺表現，如何賦予創意無限的可能性，如何從『有形的生產消費』轉變成『無形的生活價值』，才是我們追崇的夢想種子。<br /><br />用心傾聽客戶的需求，以獨一無二的風格，全然不凡的質感，為客戶開創嶄新的品牌價值與尊嚴。更以不斷突破的網路技術，令人驚艷的領航設計，來為我們的客戶服務。在這個過程中，我們堅持只保留最完美的部份給我們的客戶，而且我們總是努力不懈的設法超越客戶的期待，因為我們知道你需要什麼！</div>
                    <div class="pic full jqimgFill go fade-up">
                        <img src="{{$img}}/about/we-do02.jpg" draggable="false" alt="We create, We think, We design">
                    </div>
                </li>
            </ul>
            <div class="word eng2 h1 go fade-in" data-bottom-top="transform:translateY(-100px);" data-top-center="transform:translateY(0px);">We deliver cutting edge solutions for our clients by always putting User Experience first</div>
        </div>
    </div>
</div>
<div class="our-service dark">
    <div class="bg full">
        <div class="move jqimgFill" data-bottom-top="transform:translateY(-20%);" data-top-center="transform:translateY(10%);">
            <img src="{{$img}}/about/service-bg.jpg" draggable="false" alt="Our Service">
        </div>
    </div>
    <div class="container">
        <h2 class="d-none">Our Service</h2>
        <div class="middle">
            <div class="title eng2 display-3 tlt-loop go fade-down">Our Service</div>
            <div class="text h2 tlt go fade-up">重新思考品牌並創造明確的品牌體驗</div>
            <a href="service" class="button bling go fade-up">VIIEW MORE</a>
        </div>
    </div>
</div>
<div class="our-team dark">
    <div class="bg full jqimgFill">
        <img src="{{$img}}/about/team-bg.jpg" draggable="false" alt="Our Team">
    </div>
    <h2 class="d-none">Our Team</h2>
    <div class="container">
        <div class="title eng2 display-3 tlt-loop go fade-down">Our Team</div>
        <div class="sub eng2 h2 tlt go fade-up">We are designers, engineers, and strategists.</div>
        <ul class="photos list-unstyled">
            <li>
                <div class="pic jqimgFill">
                    <img src="{{$img}}/about/team01.jpg" draggable="false" alt="We are designers, engineers, and strategists.">
                </div>
            </li>
            <li>
                <div class="pic jqimgFill">
                    <img src="{{$img}}/about/team02.jpg" draggable="false" alt="We are designers, engineers, and strategists.">
                </div>
            </li>
            <li>
                <div class="pic jqimgFill">
                    <img src="{{$img}}/about/team03.jpg" draggable="false" alt="We are designers, engineers, and strategists.">
                </div>
            </li>
            <li>
                <div class="pic jqimgFill">
                    <img src="{{$img}}/about/team04.jpg" draggable="false" alt="We are designers, engineers, and strategists.">
                </div>
            </li>
            <li>
                <div class="pic jqimgFill">
                    <img src="{{$img}}/about/team05.jpg" draggable="false" alt="We are designers, engineers, and strategists.">
                </div>
            </li>
            <li>
                <div class="pic jqimgFill">
                    <img src="{{$img}}/about/team06.jpg" draggable="false" alt="We are designers, engineers, and strategists.">
                </div>
            </li>
            <li>
                <div class="pic jqimgFill">
                    <img src="{{$img}}/about/team07.jpg" draggable="false" alt="We are designers, engineers, and strategists.">
                </div>
            </li>
            <li>
                <div class="pic jqimgFill">
                    <img src="{{$img}}/about/team08.jpg" draggable="false" alt="We are designers, engineers, and strategists.">
                </div>
            </li>
            <li>
                <div class="pic jqimgFill">
                    <img src="{{$img}}/about/team09.jpg" draggable="false" alt="We are designers, engineers, and strategists.">
                </div>
            </li>
            <li>
                <div class="pic jqimgFill">
                    <img src="{{$img}}/about/team10.jpg" draggable="false" alt="We are designers, engineers, and strategists.">
                </div>
            </li>
            <li>
                <div class="pic jqimgFill">
                    <img src="{{$img}}/about/team11.jpg" draggable="false" alt="We are designers, engineers, and strategists.">
                </div>
            </li>
            <li>
                <div class="pic jqimgFill">
                    <img src="{{$img}}/about/team12.jpg" draggable="false" alt="We are designers, engineers, and strategists.">
                </div>
            </li>
            <li>
                <div class="pic jqimgFill">
                    <img src="{{$img}}/about/team13.jpg" draggable="false" alt="We are designers, engineers, and strategists.">
                </div>
            </li>
            <li>
                <div class="pic jqimgFill">
                    <img src="{{$img}}/about/team14.jpg" draggable="false" alt="We are designers, engineers, and strategists.">
                </div>
            </li>
            <li>
                <div class="pic jqimgFill">
                    <img src="{{$img}}/about/team15.jpg" draggable="false" alt="We are designers, engineers, and strategists.">
                </div>
            </li>
            <li>
                <div class="pic jqimgFill">
                    <img src="{{$img}}/about/team16.jpg" draggable="false" alt="We are designers, engineers, and strategists.">
                </div>
            </li>
            <li>
                <div class="pic jqimgFill">
                    <img src="{{$img}}/about/team17.jpg" draggable="false" alt="We are designers, engineers, and strategists.">
                </div>
            </li>
            <li>
                <div class="pic jqimgFill">
                    <img src="{{$img}}/about/team18.jpg" draggable="false" alt="We are designers, engineers, and strategists.">
                </div>
            </li>
            <li>
                <div class="pic jqimgFill">
                    <img src="{{$img}}/about/team19.jpg" draggable="false" alt="We are designers, engineers, and strategists.">
                </div>
            </li>
            <li>
                <div class="pic jqimgFill">
                    <img src="{{$img}}/about/team01.jpg" draggable="false" alt="We are designers, engineers, and strategists.">
                </div>
            </li>
            <li>
                <div class="pic jqimgFill">
                    <img src="{{$img}}/about/team02.jpg" draggable="false" alt="We are designers, engineers, and strategists.">
                </div>
            </li>
            <li>
                <div class="pic jqimgFill">
                    <img src="{{$img}}/about/team03.jpg" draggable="false" alt="We are designers, engineers, and strategists.">
                </div>
            </li>
            <li>
                <div class="pic jqimgFill">
                    <img src="{{$img}}/about/team04.jpg" draggable="false" alt="We are designers, engineers, and strategists.">
                </div>
            </li>
            <li>
                <div class="pic jqimgFill">
                    <img src="{{$img}}/about/team05.jpg" draggable="false" alt="We are designers, engineers, and strategists.">
                </div>
            </li>
        </ul>
        <div class="text h4 go fade-up">我們的全方位服務創意工作團隊，經過數十年的經驗磨練，所有堅持，只為用心做的更好。</div>
        <a href="{{$jobLink}}" target="_blank" class="big-button bling go fade-up">JOIN US</a>
    </div>
</div>
@endsection