@extends('frontend.layouts.app')
@section('wpClass', 'bg-black header-light')
@section('pageId', 'service')
@section('content')
    <section class="service web">
        <h2 class="d-none">Web Design 企業品牌網站建設</h2>
        <div class="bg-pic go fade-in">
            <div class="pic one" data-bottom-top="transform:translateY(-20%);" data-top-center="transform:translateY(10%);">
                <img src="{{$img}}/service/web-design01.png" draggable="false" alt="Web Design 企業品牌網站建設">
            </div>
            <div class="pic two" data-bottom-top="transform:translateY(20%);" data-top-center="transform:translateY(-10%);">
                <img src="{{$img}}/service/web-design02.png" draggable="false" alt="Web Design 企業品牌網頁設計">
            </div>
            <div class="pic three" data-bottom-top="transform:translateY(-20%);" data-top-center="transform:translateY(10%);">
                <img src="{{$img}}/service/web-design03.png" draggable="false" alt="Web Design 台中網頁設計">
            </div>
            <div class="pic four" data-bottom-top="transform:translateY(20%);" data-top-center="transform:translateY(-10%);">
                <img src="{{$img}}/service/web-design04.png" draggable="false" alt="Web Design 台北網頁設計">
            </div>
        </div>
        <div class="container">
            <div class="text">
                <div class="middle">
                    <div class="eng2 h4 go fade-down">Web Design</div>
                    <div class="title h1 tlt-loop go fade-in">企業品牌網站建設</div>
                    <div class="summary go fade-up">我們相信高質量的品牌和有效的廣告不僅僅需要服務提供商來完成這項工作。它需要一個能傾聽並理解客戶需求的合作夥伴，主動提出建議，充分了解你的需求和想法時刻掌握最新技術動向，打造高品質網站。</div>
                    <a href="works" class="button bling go fade-up">VIEW PROJECTS</a>
                </div>
            </div>
        </div>
    </section>
    <section class="service user">
        <h2 class="d-none">User Experience Design 移動設備應用設計及開發</h2>
        <div class="bg-pic go fade-in">
            <div class="pic one" data-bottom-top="transform:translateX(0%);" data-top-center="transform:translateX(10%);">
                <img src="{{$img}}/service/user01.png" draggable="false" alt="響應式網頁設計">
            </div>
            <div class="pic two" data-bottom-top="transform:translateX(10%);" data-top-center="transform:translateX(0%);">
                <img src="{{$img}}/service/user02.png" draggable="false" alt="移動設備應用設計及開發">
            </div>
            <div class="pic three" data-bottom-top="transform:translateY(-20%);" data-top-center="transform:translateY(10%);">
                <img src="{{$img}}/service/user03.png" draggable="false" alt="系統整合應用開發">
            </div>
        </div>
        <div class="container">
            <div class="text">
                <div class="middle">
                    <div class="eng2 h4 go fade-down">User Experience Design</div>
                    <div class="title h1 tlt-loop go fade-in">移動設備應用設計及開發</div>
                    <div class="summary go fade-up">通過把用戶放在第一位，創造出無縫，直觀的產品體驗，用設計風格體現企業文化、追求速度，兼容主流手機瀏覽器和 PC 端數據同步。</div>
                    <a href="works" class="button bling go fade-up">VIEW PROJECTS</a>
                </div>
            </div>
        </div>
    </section>
    <section class="service photography">
        <h2 class="d-none">商業攝影</h2>
        <div class="bg-pic full">
            <div class="move jqimgFill" data-bottom-top="transform:translateY(-20%);" data-top-center="transform:translateY(10%);">
                <img src="{{$img}}/service/photography-bg.jpg" draggable="false" alt="商業攝影">
            </div>
        </div>
        <div class="container">
            <div class="text">
                <div class="middle">
                    <div class="title h1 tlt-loop go fade-in">商業攝影</div>
                    <div class="summary go fade-up">堅持最佳的影像品質及服務，以提升客戶的銷售成績和完美的商業形象。好的商品拍攝絕對是您吸引客人目光的一大要件。</div>
                    <a href="works" class="button bling go fade-up">VIEW PROJECTS</a>
                </div>
            </div>
        </div>
    </section>
    <section class="service plan">
        <h2 class="d-none">文案企劃＆品牌戰略</h2>
        <div class="bg-pic full">
            <div class="move jqimgFill" data-bottom-top="transform:translateY(-20%);" data-top-center="transform:translateY(10%);">
                <img src="{{$img}}/service/plan-bg.jpg" draggable="false" alt="文案企劃＆品牌戰略">
            </div>
        </div>
        <div class="container">
            <div class="text">
                <div class="middle">
                    <div class="title h1 tlt-loop go fade-in">文案企劃＆品牌戰略</div>
                    <div class="summary go fade-up">人類所有的發明都來自於創意想法，而創意想法是通過文字、語言交流來互相傳播的。好的文案則需通過對話來塑造和完善。<br>這時候，您需要的是一位文字化妝師。</div>
                    <a href="works" class="button bling go fade-up">VIEW PROJECTS</a>
                </div>
            </div>
        </div>
    </section>
@endsection