@extends('frontend.layouts.app')
@section('wpClass', 'bg-black header-light')
@section('pageId', 'home')
@section('content')

    <section class="bg-video">
        {{--<div class="pic jqimgFill">--}}
            {{--<img src="{{$img}}/home/bg.jpg" draggable="false" alt="雲端數位">--}}
        {{--</div>--}}
    </section>

    <section class="home-banner">
        <div class="triangles">
            <div class="top" data-bottom-top="transform:translateY(-20%);" data-top-center="transform:translateY(0%);"></div>
            <div class="right" data-bottom-top="transform:translateX(-10%);" data-top-center="transform:translateX(0%);"></div>
            <div class="left" data-bottom-top="transform:translateX(0%);" data-top-center="transform:translateX(-10%);"></div>
        </div>
        <div class="container">
            <h2 class="d-none">We Design User Experience</h2>
            <div class="title display-1 eng" data-bottom-top="transform:translateY(0px);" data-top-center="transform:translateY(-80px);">
                <div class="tlt-loop">We Design</div>
                <div class="tlt-loop">User Experience</div>
            </div>
            <div class="slider" data-bottom-top="transform:translateY(-10%);" data-top-center="transform:translateY(10%);">
                <div class="pic">
                    <img src="{{$img}}/home/banner01.png" draggable="false" alt="We Design User Experience">
                </div>
                <div class="pic">
                    <img src="{{$img}}/home/banner02.png" draggable="false" alt="We Design User Experience">
                </div>
            </div>
            <div class="text tlt display-4">量身打造全新的 UI 及用戶體驗設計</div>
        </div>
    </section>

    <section class="project has-animation">
        <h2 class="d-none">Wroks 作品賞析</h2>
        <ul class="list list-unstyled clearfix">
            @foreach ($workIndexs as $workIndex)
                <li>
                    <a class="box" href="./works/detail/{{ $workIndex->pageName }}" title="{{$companyName}} - {{ $workIndex->subject }}">
                        <span class="box-line"></span>
                        <span class="pic jqimgFill">
                                <img src="{{ $img }}/{{ $workIndex->medias->banner[0]->fileName }}?w=960&fit=crop" draggable="false" alt="{{$workDatailName}} - {{ $value->title or $workIndex->subject }}">
                        </span>
                        <span class="text">
                            <h3 class="name h2">{{ $workIndex->subject }}</h3>
                            <span class="summary">{{ $workIndex->service }}</span>
                            <span class="btn" title="作品介紹">
                                <i class="icon-plus"></i>
                                <span class="d-none">作品介紹</span>
                            </span>
                        </span>
                    </a>
                </li>
            @endforeach
        </ul>
    </section>
    <section class="vision">
        <div class="bg full">
            <div class="move jqimgFill" data-bottom-top="transform:translateY(-20%);" data-top-center="transform:translateY(30%);">
                <img src="{{ $img }}/home/vision-bg.jpg" draggable="false" alt="Why MINMAX?">
            </div>
        </div>
        <h2 class="d-none">Why MINMAX?</h2>
        <div class="info">
            <div class="container">
                <a href="works/detail/m000257" class="button bling go fade-up">MORE WORK</a>
                <div class="title display-2 eng tlt-loop go fade-up">Why MINMAX?</div>
                <div class="text tlt h1 go fade-up">知識、經驗與創新的想法，我們知道你需要甚麼！</div>
            </div>
        </div>
        <div class="block">
            <div class="container">
                <div class="table-box">
                    <div class="table-tr">
                        <div class="table-td item go fade-down">
                            <div class="total"><span class="number run-number display-3 eng" data-number="10">0</span>年</div>
                            <div class="sub">不斷堅持創新</div>
                        </div>
                        <div class="table-td item has-shadow go fade-down">
                            <div class="total"><span class="number run-number display-3 eng" data-number="200">0</span>+</div>
                            <div class="sub">用戶體驗設計項目</div>
                        </div>
                        <div class="table-td slogan h1 go fade-right">堅持，只為用心做的更好</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="photo">
            <div class="pic jqimgFill go fade-up">
                <img src="{{ $img }}/home/vision01.jpg" draggable="false" alt="Why MINMAX?">
            </div>
            <div class="pic jqimgFill go fade-down">
                <img src="{{ $img }}/home/vision02.jpg" draggable="false" alt="Why MINMAX?">
            </div>
        </div>
        <div class="block bottom">
            <div class="container">
                <div class="table-box">
                    <div class="table-tr">
                        <div class="table-td slogan h2 go fade-left">創造出色的用戶體驗<br />為合作企業創造更大的商業價值</div>
                        <div class="table-td item go fade-up">
                            <div class="total"><span class="number run-number f72 eng" data-number="850">0</span>+</div>
                            <div class="sub">我們引以為傲的合作企業</div>
                        </div>
                        <div class="table-td item has-shadow go fade-up">
                            <div class="total"><span class="number run-number f72 eng" data-number="100">0</span>+</div>
                            <div class="sub">上市櫃企業合作</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="client">
        <div class="container">
            <h2 class="d-none">服務客戶</h2>
            <ul class="slider list-unstyled go fade-up">
                @foreach ($clientIndexs as $clientIndex)
                <li>
                    <div class="logo">
                        <img src="{{ $img }}/{{ $clientIndex->medias }}" draggable="false" alt="{{ $clientIndex->subject }}">
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </section>
@endsection