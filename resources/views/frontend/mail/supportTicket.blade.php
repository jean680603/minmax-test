<div style="background:#f0f0f0;font:12px Arial,微軟正黑體;color:#000;padding:30px">
    <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="background:#fff;font:12px Arial,微軟正黑體;color:#000;border:10px #e0e0e0 solid;border-radius:20px">
        <tbody><tr>
            <td align="left" valign="top" style="padding:20px">
                <p style="color:#000;font-size:16px;font-weight:bold;margin:10px 0 20px">
                    親愛的 <span style="color:#125773">{{$input['name']}}</span> ，您好
                </p>
                <div style="border:1px dashed #ccc;border-left:none;border-right:none;padding:25px 0">
                    <p style="margin:0 20px">
                        您的來信我們已經收到。請耐心等候一些時間，<wbr>我們將會盡快回覆您的問題。<br>
                        本次信件單號為 <b style="color:#125773">{{$input['serial']}}</b>，如果您需要專人立即服務，<wbr>請直接與我們的客服中心聯繫。
                    </p>
                    <p style="margin:20px"><b>以下為您的信件資料明細：</b></p>
                    <p style="margin:20px">
                        尊姓大名：{{$input['name']}}<br>
                        聯絡電話：{{$input['phone']}}<br>
                        電子信箱：<a href="mailto:{{$input['mail']}}" target="_blank">{{$input['mail']}}</a><br>
                        訊息時間：{{$input['ip']}}<br>
                        訊息位置：{{$input['time']}}<br>
                        信件主旨：{{$input['subject']}}<br>
                        信件內容：{{$input['content']}}
                    </p>
                    <p style="margin:20px">
                        -----<br>
                        客服專線：<a href="tel:{{$companyPhone}}">{{$companyPhone}}</a><br>
                        客服信箱：<a href="mailto:{{$companyEmail}}">{{$companyEmail}}</a><br>
                        客服經理：{{$companyCall}} / Jean
                    </p>
                    <p style="margin:0 20px">
                        謝謝您對{{$companyName}}的支持<br>
                        敬祝 順利 平安
                    </p>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center" valign="top">
                <div style="font-size:14px;font-weight:bold;color:#125773;margin:0">本信件為系統自動發送，請勿直接回覆！</div>
                <p style="font-family:Tahoma,Geneva,sans-serif;font-size:12px;color:#666;margin:5px 0 25px">
                    Copyright © <a href="{{$websiteUrl}}" style="color:#0066cc" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=zh-TW&amp;q={{$websiteUrl}}&amp;source=gmail&amp;ust=1537363837008000&amp;usg=AFQjCNG0FzISz3LehGni-r_dn7YQHl_Qqw">{{$companyName}}</a>,  All rights reserved.
                </p>
            </td>
        </tr>
        </tbody></table><div class="yj6qo"></div><div class="adL">
    </div></div>