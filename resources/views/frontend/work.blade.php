
@extends('frontend.layouts.app')
@section('wpClass', '')
@section('title', "網頁設計作品-" . $work->subject)
@section('pageId', 'projects')
@section('content')


    <div class="prevNext">
        @if ($perPage)
        <a class="prev" href="/works/detail/{{$perPage->pageName}}" title="{{$workDatailName}} - {{$perPage->subject}}">
            <span class="pic jqimgFill">
                <img src="{{$img}}/{{$perPage->medias->banner[0]->fileName}}" draggable="false" alt="{{$workDatailName}} - {{$perPage->subject}}">
            </span>
            <span class="text eng2">PREV</span>
        </a>
        @endif
        @if ($pageNext)
        <a class="next" href="/works/detail/{{$pageNext->pageName}}" title="{{$workDatailName}} - {{$pageNext->subject}}">
            <span class="pic jqimgFill">
                <img src="{{$img}}/{{$pageNext->medias->banner[0]->fileName}}" draggable="false" alt="{{$workDatailName}} - {{$pageNext->subject}}">
            </span>
            <span class="text eng2">NEXT</span>
        </a>
        @endif
    </div>
    <section class="in-banner">
        <h2 class="d-none">{{$work->subject}}</h2>
        <div class="bg full go fade-in">
            <div class="move full jqimgFill" data-bottom-top="transform:translateY(150px);" data-top-center="transform:translateY(-100px);">
                    <img src="{{$img}}/{{ $work->medias->banner[0]->fileName }}" draggable="false" alt="{{$workDatailName}} - {{ $work->medias->banner[0]->title or $work->subject }}">
            </div>
        </div>
        <div class="container">
            <div class="middle">
                <div class="title display-4 tlt-loop go fade-down">{{$work->subject}}</div>
                <div class="text go fade-up">{{$work->description}}</div>
                <div class="bottom clearfix go fade-up">
                    <ul class="list-unstyled">
                        <li>Services</li>
                        <li>{{$work->tags}}</li>
                    </ul>
                    @if($work->url)
                        <a href="{{$work->url}}" target="_blank" class="wiew" title="瀏覽 {{$work->subject}} 網站">View site</a>
                    @endif

                </div>
            </div>
        </div>
        <div class="goDown"></div>
    </section>
    <div class="project-mac">
        <div class="scroll-here"></div>
        <div class="container">
            <div class="text go fade-down">{{$work->content}}</div>
            <div class="share go fade-up a2a_kit a2a_kit_size_32 a2a_default_style">
                <a class="to-facebook a2a_button_facebook"><i class="icon-facebook"></i></a>
                <a class="to-twitter a2a_button_twitter"><i class="icon-twitter"></i></a>
                <a class="to-google a2a_button_google_plus"><i class="icon-gplus"></i></a>
                <a class="to-line a2a_button_line"><i class="icon-line"></i></a>
            </div>
        </div>
        <div class="block go fade-in" data-bottom-top="transform:translateY(-5%);" data-top-center="transform:translateY(10%);">
            <div class="mac">
                <div class="slider">
                    @foreach($work->medias->desktop as $key => $value)
                        <div class="pic">
                            <img src="{{$img}}/{{ $value->fileName }}" draggable="false" alt="{{$workDatailName}} - @if ($value->title){{$value->title}}@else {{$work->subject}} @endif">
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
    <div class="project-index">
        <div class="container go fade-up">
              <img src="{{$img}}/{{ $work->medias->img[0]->fileName }}" draggable="false" alt="{{$workDatailName}} - @if ($work->medias->img[0]->title){{$work->medias->img[0]->title}}@else {{$work->subject}} @endif">
        </div>
    </div>
    @if ($work->medias->img[0])
    <div class="project-page light">
        <div class="container go fade-up">
            <div class="pic go">
            @foreach($workMedias->img as $key => $value)
                @if ($loop->index >= 1 and $loop->index%2 == 1)
                    <img src="{{$img}}/{{ $value->fileName }}" draggable="false" alt="{{$workDatailName}} - @if ($value->title){{$value->title}}@else {{$work->subject}} @endif">
                @endif
            @endforeach
            </div>

            <div class="pic go">
            @foreach($workMedias->img as $key => $value)
                @if ($loop->index >= 1 and $loop->index%2 == 0)
                    <img src="{{$img}}/{{ $value->fileName }}" draggable="false" alt="{{$workDatailName}} - @if ($value->title){{$value->title}}@else {{$work->subject}} @endif">
                @endif
            @endforeach
            </div>
        </div>
    </div>
    @endif
    @if ($work->medias->mobile[0])
    <div class="project-phone dark">
        <div class="container"><!-- animated-left / animated-right / animated-up / animated-down -->
                <div class="phone go fade-left">
                    <div class="pic jqimgFill go">
                        <img src="{{$img}}/{{ $work->medias->mobile[0]->fileName }}" draggable="false" alt="{{$workDatailName}} - {{ $work->medias->mobile[0]->title or $work->subject }}">
                    </div>
                </div>
                <div class="phone go fade-up">
                    <div class="pic jqimgFill go {{ $work->medias->mobile[1]->class }}">
                        <img src="{{$img}}/{{ $work->medias->mobile[1]->fileName }}" draggable="false" alt="{{$workDatailName}} - {{ $work->medias->mobile[1]->title or $work->subject }}">
                    </div>
                </div>
                <div class="phone go fade-right">
                    <div class="pic jqimgFill go {{ $work->medias->mobile[2]->class }}">
                        <img src="{{$img}}/{{ $work->medias->mobile[2]->fileName }}" draggable="false" alt="{{$workDatailName}} - {{ $work->medias->mobile[2]->title or $work->subject }}">
                    </div>
                </div>
        </div>
    </div>
    @endif
    @if ($work->medias->ipad[0])
    <div class="project-pad light">
        <div class="container-full go fade-up">
            <div class="pad black" data-bottom-top="transform:translateX(-100px);" data-top-center="transform:translateX(0px);">
                <div class="pic jqimgFill">
                     <img src="{{$img}}/{{ $work->medias->ipad[0]->fileName  }}" draggable="false" alt="{{$workDatailName}} - {{ $work->medias->ipad[0]->title or $work->subject }}">
                </div>
            </div>
            <div class="pad white" data-bottom-top="transform:translateX(100px);" data-top-center="transform:translateX(0px);">
                <div class="pic jqimgFill">
                  <img src="{{$img}}/{{ $work->medias->ipad[1]->fileName }}" draggable="false" alt="{{$workDatailName}} - {{ $work->medias->ipad[1]->title or $work->subject }}">
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="project has-animation dark">
        <h3 class="d-none">其他作品賞析</h3>
        <ul class="list list-unstyled clearfix">
            @if ($perPage)
            <li class="all brand export">
                <a class="box" href="/works/detail/{{$perPage->pageName}}">
                    <span class="box-line"></span>
                    <span class="pic jqimgFill">
                        <img src="{{$img}}/{{$perPage->medias->banner[0]->fileName}}"  draggable="false" alt="{{$perPage->subject}}">
                    </span>
                    <span class="text scrollbar">
                        <h3 class="name h1">{{$perPage->subject}}</h3>
                        <span class="summary">{{$perPage->tags}}</span>
                        <span class="btn" title="作品介紹">
                            <i class="icon-plus"></i>
                            <span class="d-none">作品介紹</span>
                        </span>
                    </span>
                </a>
            </li>
            @else
                <li class="all brand export">
                    <a class="box" href="/works/detail/{{$work->pageName}}">
                        <span class="box-line"></span>
                        <span class="pic jqimgFill">
                        <img src="{{$img}}/{{$work->medias->banner[0]->fileName}}"  draggable="false" alt="{{$work->subject}}">
                    </span>
                        <span class="text scrollbar">
                        <h3 class="name h1">{{$work->subject}}</h3>
                        <span class="summary">{{$work->tags}}</span>
                        <span class="btn" title="作品介紹">
                            <i class="icon-plus"></i>
                            <span class="d-none">作品介紹</span>
                        </span>
                    </span>
                    </a>
                </li>
            @endif
                @if ($pageNext)
            <li class="all brand">
                <a class="box" href="/works/detail/{{$pageNext->pageName}}">
                    <span class="box-line"></span>
                    <span class="pic jqimgFill">
                        <img src="{{$img}}/{{$pageNext->medias->banner[0]->fileName}}" draggable="false" alt="{{$pageNext->subject}}">
                    </span>
                    <span class="text scrollbar">
                        <h3 class="name h1">{{$pageNext->subject}}</h3>
                        <span class="summary">{{$pageNext->tags}}</span>
                        <span class="btn" title="作品介紹">
                            <i class="icon-plus"></i>
                            <span class="d-none">作品介紹</span>
                        </span>
                    </span>
                </a>
            </li>
                @else
                    <li class="all brand">
                        <a class="box" href="/works/detail/{{$work->pageName}}">
                            <span class="box-line"></span>
                            <span class="pic jqimgFill">
                        <img src="{{$img}}/{{$work->medias->banner[0]->fileName}}"  draggable="false" alt="{{$work->subject}}">
                    </span>
                            <span class="text scrollbar">
                        <h3 class="name h1">{{$work->subject}}</h3>
                        <span class="summary">{{$work->tags}}</span>
                        <span class="btn" title="作品介紹">
                            <i class="icon-plus"></i>
                            <span class="d-none">作品介紹</span>
                        </span>
                    </span>
                        </a>
                    </li>
                @endif
        </ul>
    </div>
@endsection