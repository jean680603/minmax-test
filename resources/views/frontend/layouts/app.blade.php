<!doctype html>
<html lang="zh-TW" class="no-js">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <link rel="icon" type="image/x-icon" href="/{{$publicUrl}}/images/favicon.ico" />
    <title itemprop="name">{{$websiteName}}-@yield('title',$websiteTitle)</title>
    <meta name="author" content="{{$companyName}}" />
    <meta name="description" content="{{$websiteDescription}}" />
    <meta name="keywords" content="{{$websiteKeywords}}" />
    @yield('meta')
    <!-- Facebook Open Graph Meta -->
    <meta property="og:title" content="" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="" />
    <!-- Styles -->
    @yield('before-styles')
    <link rel="stylesheet" href="/{{$publicUrl}}/css/app.css">
    @yield('after-styles')
    <script src="/{{$publicUrl}}/js/modernizr.js"></script>



    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/zh_TW/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <script async src="https://static.addtoany.com/menu/page.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
<div class="wp @yield('wpClass')" id="@yield('pageId')">
    @section('header')
        @include('frontend.includes.header')
    @show
    <main class="main">
        <div class="inner">
            @yield('content')
        </div>
    </main>
    @section('footer')
        @include('frontend.includes.footer')
    @show
</div>
<!-- Scripts -->
@yield('before-scripts')
<script src="/{{$publicUrl}}/js/app.js"></script>
@yield('after-scripts')
</body>
</html>