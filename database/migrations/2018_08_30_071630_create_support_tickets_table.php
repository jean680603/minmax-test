<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('support_tickets', function (Blueprint $table) {
            $table->increments('id');
            //$table->integer('categoryId')->unsigned()->default('0');
            $table->string('classId','32')->nullable();
            $table->text('userInfo')->collation('utf8_unicode_ci')->nullable();
            $table->string('serial', 32)->collation('utf8_unicode_ci')->nullable();
            $table->string('subject',255)->collation('utf8_unicode_ci')->nullable();
            $table->text('content')->collation('utf8_unicode_ci')->nullable();
            $table->text('fields')->collation('utf8_unicode_ci')->nullable();
            $table->string('name', 32)->collation('utf8_unicode_ci')->nullable();
            $table->string('phone', 255)->collation('utf8_unicode_ci')->nullable();
            $table->string('mail', 255)->collation('utf8_unicode_ci')->nullable();
            $table->string('mobile', 255)->collation('utf8_unicode_ci')->nullable();
            $table->string('ip', 32)->collation('utf8_unicode_ci')->nullable();
            $table->enum('isDeleted', array('Ｙ', 'Ｎ'))->default('N')->collation('utf8_unicode_ci');
            $table->enum('isRead', array('Ｙ', 'Ｎ'))->default('N')->collation('utf8_unicode_ci');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_tickets');
    }
}
