<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article', function (Blueprint $table) {
            $table->increments('id');
            $table->string('serial', 32)->collation('utf8_unicode_ci')->nullable();//編號
            $table->integer('categoryId')->unsigned()->default('0');//分類
            $table->string('subject',255)->collation('utf8_unicode_ci');
            $table->text('description')->collation('utf8_unicode_ci')->nullable();
            $table->text('content')->collation('utf8_unicode_ci')->nullable();
            $table->text('tags')->collation('utf8_unicode_ci')->nullable();//標籤
            $table->text('medias')->collation('utf8_unicode_ci')->nullable();
            $table->text('pageName')->collation('utf8_unicode_ci')->nullable();
            $table->string('url',255)->collation('utf8_unicode_ci')->nullable();
            $table->enum('enable', array('Ｙ', 'Ｎ'))->default('Ｙ')->collation('utf8_unicode_ci');
            $table->integer('sortIndex')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article');
    }
}
