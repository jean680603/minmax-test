<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject',255)->collation('utf8_unicode_ci')->nullable();
            $table->text('fieldValue')->collation('utf8_unicode_ci')->nullable();
            $table->text('description')->collation('utf8_unicode_ci')->nullable();
            $table->text('medias')->collation('utf8_unicode_ci')->nullable();
            $table->integer('sortIndex')->nullable()->unsigned();
            $table->integer('categoryId')->unsigned()->default('0');
            $table->string('classId','32')->nullable();//模組名稱
            $table->string('customId','32')->nullable();//主層分類名稱
            $table->enum('enable', array('Ｙ', 'Ｎ'))->default('Ｙ')->collation('utf8_unicode_ci');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_category');
    }
}
