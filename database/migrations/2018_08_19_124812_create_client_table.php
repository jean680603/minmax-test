<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client', function (Blueprint $table) {
            $table->increments('id');
            $table->string('serial', 32)->collation('utf8_unicode_ci')->nullable();
            $table->string('subject',255)->collation('utf8_unicode_ci')->nullable();
            $table->text('medias')->collation('utf8_unicode_ci')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->enum('enable', array('Ｙ', 'Ｎ'))->default('Ｙ')->collation('utf8_unicode_ci');
            $table->integer('sortIndex')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client');
    }
}
