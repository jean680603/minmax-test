<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('settings')->insert([
            ['fieldName' => 'websiteName','fieldValue' => '雲端數位科技','class' => 'null','description' => 'null'],
            ['fieldName' => 'websiteUrl','fieldValue' => 'https://minmax.biz','class' => 'null','description' => 'null'],
            ['fieldName' => 'websiteEmail','fieldValue' => 'service@minmax.biz','class' => 'email','description' => '系統信箱'],
            ['fieldName' => 'websiteTitle','fieldValue' => '頂級網頁設計公司、網站跨系統整合開發與國際化網頁設計專家','class' => 'null','description' => 'null'],
            ['fieldName' => 'websiteDescription','fieldValue' => '雲端數位科技是一家充滿新鮮創意及設計熱情的網路公司。我們擁有頂尖質量、橫跨多方面領域的設計團隊，我們擅於打造以創新為本的設計作品、專業於多種領域的軟體工程技術及全方位的服務導向整合，我們深信知識與經驗的累積才是打動人心的服務。','class' => 'null','description' => 'null'],
            ['fieldName' => 'websiteKeywords','fieldValue' => '網頁設計,網站設計,RWD網頁設計,響應式網頁設計,購物網站設計,網站建置,SEO優化設計,程式設計,網頁設計公司,台北網頁設計','class' => 'null','description' => 'null'],
            ['fieldName' => 'websiteMobile','fieldValue' => '0988206362','class' => 'mobile','description' => '系統簡訊'],
            ['fieldName' => 'companyName','fieldValue' => '雲端數位科技有限公司','class' => 'null','description' => 'null'],
            ['fieldName' => 'companyId','fieldValue' => '242418972','class' => 'null','description' => 'null'],
            ['fieldName' => 'companyEmail','fieldValue' => 'service@minmax.biz','class' => 'email','description' => '客服信箱'],
            ['fieldName' => 'companyPhone','fieldValue' => '886-4-24350749','class' => 'null','description' => 'null'],
            ['fieldName' => 'companyFax','fieldValue' => '886-4-24350745','class' => 'null','description' => 'null'],
            ['fieldName' => 'companyCall','fieldValue' => '0987000588','class' => 'null','description' => 'null'],
            ['fieldName' => 'companyAddress','fieldValue' => '406 台中市北屯區東山路一段147巷49號','class' => 'null','description' => 'null'],
            ['fieldName' => 'companyGoogleMap','fieldValue' => 'https://goo.gl/maps/Vgb8xTW225R2','class' => 'null','description' => 'null'],
            ['fieldName' => 'gaUaNumber','fieldValue' => 'UA-78692538-1','class' => 'null','description' => 'null'],
            ['fieldName' => 'gaGaNumber','fieldValue' => 'null','class' => 'null','description' => 'null'],
            ['fieldName' => 'facebook','fieldValue' => 'https://m.me/minmaxeric','class' => 'null','description' => 'null'],
            ['fieldName' => 'line','fieldValue' => 'https://line.me/ti/p/TTMZwDsULz','class' => 'null','description' => 'null'],
            ['fieldName' => 'skype','fieldValue' => 'skype:lin_chi_an?chat','class' => 'null','description' => 'null'],
            ['fieldName' => 'wechat','fieldValue' => 'null','class' => 'null','description' => 'null'],
            ['fieldName' => 'copyright','fieldValue' => 'COPYRIGHT © 2018 MINMAX 網頁設計','class' => 'null','description' => 'null'],
            ['fieldName' => 'socialFacebook','fieldValue' => 'https://zh-tw.facebook.com/minmaxeric/','class' => 'null','description' => 'null'],
            ['fieldName' => 'jobLink','fieldValue' => 'https://www.104.com.tw/jobbank/custjob/index.php?r=cust&j=4070432838363f6940583a1d1d1d1d5f2443a363189j48','class' => 'null','description' => 'null'],

        ]);
    }
}
