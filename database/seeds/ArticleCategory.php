<?php

use Illuminate\Database\Seeder;

class ArticleCategory extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('article_category')->insert([

            [
                'subject' => '上市上櫃',
                'sortIndex' =>'1',
                'classId' => 'articleWork',
                'enable'=> 'Y',
                'fieldValue'=> 'export',

            ],[
                'subject' => '企業品牌',
                'sortIndex' =>'2',
                'classId' => 'articleWork',
                'enable'=> 'Y',
                'fieldValue'=> 'brand',
            ],
            [
                'subject' => '購物電商',
                'sortIndex' =>'3',
                'classId' => 'articleWork',
                'enable'=> 'Y',
                'fieldValue'=> 'shopping',

            ],
            [
                'subject' => '工業外銷',
                'sortIndex' =>'4',
                'classId' => 'articleWork',
                'enable'=> 'Y',
                'fieldValue'=> 'export',

            ]

         ]);
    }
}
