<?php

use Illuminate\Database\Seeder;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('client')->insert(
            [
            [
                'serial' => 'm000216',
                'subject' => '嘉鴻遊艇集團',
                'medias' => 'm000216-logo.png',
                'enable' => 'Y',
                'sortIndex' => '2',
            ],[
                'serial' => 'm000238',
                'subject' => '台灣山葉機車工業股份有限公司',
                'medias' => 'm000238-logo.png',
                'enable' => 'Y',
                'sortIndex' => '1',
            ],[
                'serial' => 'm000192',
                'subject' => '黑橋牌企業股份有限公司',
                'medias' => 'm000192-logo.png',
                'enable' => 'Y',
                'sortIndex' => '1',
            ],[
                'serial' => 'm000257',
                'subject' => '恆隆行貿易股份有限公司',
                'medias' => 'm000257-logo.png',
                'enable' => 'Y',
                'sortIndex' => '1',
            ],[
                'serial' => 'm000186',
                'subject' => '明台產物保險',
                'medias' => 'm000186-logo.png',
                'enable' => 'Y',
                'sortIndex' => '1',
            ],[
                'serial' => 'm000211',
                'subject' => '宏泰人壽保險',
                'medias' => 'm000211-logo.png',
                'enable' => 'Y',
                'sortIndex' => '2',
            ],[
                'serial' => 'e000363',
                'subject' => '鼎泰豐',
                'medias' => 'e000363-logo.png',
                'enable' => 'Y',
                'sortIndex' => '1',

            ],[
                'serial' => 'm000202',
                'subject' => '正隆股份有限公司',
                'medias' => 'm000202-logo.png',
                'enable' => 'Y',
                'sortIndex' => '1',
            ],[
                'serial' => 'm000259',
                'subject' => '中國醫藥大學附設醫院 臨床試驗中心',
                'medias' => 'm000259-logo.png',
                'enable' => 'Y',
                'sortIndex' => '1',
            ],[
                'serial' => 'm000250',
                'subject' => '大樹藥局',
                'medias' => 'm000250-logo.png',
                'enable' => 'Y',
                'sortIndex' => '1',
            ],[
                'serial' => 'e000443',
                'subject' => '九乘九文具',
                'medias' => 'e000443-logo.png',
                'enable' => 'Y',
                'sortIndex' => '1',
            ],[
                'serial' => 'e000441',
                'subject' => '奇美新視代',
                'medias' => 'e000441-logo.png',
                'enable' => 'Y',
                'sortIndex' => '1',
            ],[
                'serial' => 'm000265',
                'subject' => '世界健身事業有限公司',
                'medias' => 'm000265-logo.png',
                'enable' => 'Y',
                'sortIndex' => '1',
            ],[
                'serial' => 'm000194',
                'subject' => '長庚醫院',
                'medias' => 'm000194-logo.png',
                'enable' => 'Y',
                'sortIndex' => '1',
            ],[
                'serial' => 'm000227',
                'subject' => '紫金堂',
                'medias' => 'm000227-logo.png',
                'enable' => 'Y',
                'sortIndex' => '2',
            ],[
                'serial' => 'm000231',
                'subject' => '悠遊卡股份有限公司',
                'medias' => 'm000231-logo.png',
                'enable' => 'Y',
                'sortIndex' => '1',
            ],[
                'serial' => 'm000262',
                'subject' => '麗明營造股份有限公司',
                'medias' => 'm000262-logo.png',
                'enable' => 'Y',
                'sortIndex' => '1',
            ],[
                'serial' => 'e000438',
                'subject' => '聯發科技股份有限公司',
                'medias' => 'e000438-logo.png',
                'enable' => 'Y',
                'sortIndex' => '1',
            ],[
                'serial' => 'm000230',
                'subject' => '太平洋崇光百貨',
                'medias' => 'm000230-logo.png',
                'enable' => 'Y',
                'sortIndex' => '1',
            ],[
                'serial' => 'm000218',
                'subject' => '弘爺漢堡',
                'medias' => 'm000218-logo.png',
                'enable' => 'Y',
                'sortIndex' => '2',
            ],[
                'serial' => 'e000386',
                'subject' => '福壽新實業股份有限公司',
                'medias' => 'e000386-logo.png',
                'enable' => 'Y',
                'sortIndex' => '3',
            ],[
                'serial' => 'm000245',
                'subject' => '環鴻科技股份有限公司',
                'medias' => 'm000245-logo.png',
                'enable' => 'Y',
                'sortIndex' => '3',
            ],[
                'serial' => 'e000384',
                'subject' => '八方雲集',
                'medias' => 'e000384-logo.png',
                'enable' => 'Y',
                'sortIndex' => '2',
            ],[
                'serial' => 'e000427',
                'subject' => '葡萄王生技股份有限公司',
                'medias' => 'e000427-logo.png',
                'enable' => 'Y',
                'sortIndex' => '1',
            ],[
                'serial' => 'e000407',
                'subject' => '寶島鐘錶股份有限公司',
                'medias' => 'e000407-logo.png',
                'enable' => 'Y',
                'sortIndex' => '1',
            ],[
                'serial' => 'e000376',
                'subject' => '杏輝藥品工業股份有限公司',
                'medias' => 'e000376-logo.png',
                'enable' => 'Y',
                'sortIndex' => '1',
            ],[
                'serial' => 'm000223',
                'subject' => '冠軍磁磚',
                'medias' => 'm000223-logo.png',
                'enable' => 'Y',
                'sortIndex' => '1',
            ],[
                'serial' => 'm000166',
                'subject' => '倫飛電腦',
                'medias' => 'm000166-logo.png',
                'enable' => 'Y',
                'sortIndex' => '1',
            ]
        ]);

    }
}
